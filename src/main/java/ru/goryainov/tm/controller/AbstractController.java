package ru.goryainov.tm.controller;

import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.enumerated.Role;

import java.util.Scanner;

public class AbstractController {

    private final Scanner scanner = new Scanner(System.in);

    protected String scanNextLine() {
        return scanner.nextLine();
    }

    protected Long scanNextLong() {
        try {
            return scanner.nextLong();
        } finally {
            scanner.nextLine();
        }
    }

    protected Integer scanNextInt() {
        try {
            return scanner.nextInt();
        } finally {
            scanner.nextLine();
        }
    }

    /*public boolean checkSession(UUID sessionId){
     *//*if (sessionId == null) {
            System.out.println("Access denied! Please, log in...");
            return false;}
        return true;*//*
        boolean checkAuth = sessionId == null;
        if(checkAuth){
            System.out.println("Access denied! Please, log in...");
        }
        return checkAuth;
    }*/

    public boolean checkAdminGrants(final User user) {
        if (user.getRole().toString().equals(Role.ADMIN.toString())) return true;
        return false;
    }

}
