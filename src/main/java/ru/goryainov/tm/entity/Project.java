package ru.goryainov.tm.entity;

public class Project {

    /**
     * Идентификатор экземпляра класса с инициализацией случайным числом
     */
    private Long id = System.nanoTime();

    /**
     * Имя проекта
     */
    private String name = "";

    /**
     * Описание проекта
     */
    private String description = "";

    /**
     * Связь с пользователем по идентификатору пользователя
     */
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }
}
