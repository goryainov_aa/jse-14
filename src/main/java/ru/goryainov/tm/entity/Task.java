package ru.goryainov.tm.entity;

public class Task {

    /**
     * Идентификатор экземпляра класса с инициализацией случайным числом
     */
    private Long id = System.nanoTime();

    /**
     * Имя проекта
     */
    private String name = "";

    /**
     * Описание проекта
     */
    private String description = "";

    /**
     * Связь с проектом по идентификатору проекта
     */
    private Long projectId;

    /**
     * Связь с пользователем по идентификатору пользователя
     */
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }
}
